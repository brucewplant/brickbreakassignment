#include "shrinkingbrick.h"



void ShrinkingBrick::collision(Ball &ball)
{
    Brick::collision(ball);

    m_position.setX(m_position.x() + m_width * .125);
    m_position.setY(m_position.y() + m_height * .125);
    m_width *= .75;
    m_height *= .75;
}
