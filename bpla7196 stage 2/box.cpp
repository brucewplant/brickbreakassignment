#include "box.h"

Box *Box::instance = 0;

Box *Box::getInstance(int xPosition, int yPosition, int width, int height)
{
    // Singleton design pattern.
    if (instance == 0) {
        instance = new Box(xPosition, yPosition, width, height);
    }
    return instance;



}

void Box::render(QPainter &painter, double time)
{
    // Draw the box...
    drawBox(painter);

    // Draw the ball...
    updateBalls(time);

    double maxVelocity = 0;

    for (int i = 0; i < (int) m_balls.size(); i++) {
        if (m_balls[i]->getVelocity().length() > maxVelocity) {
            maxVelocity = m_balls[i]->getVelocity().length();
        }
    }

    for (int i = 0; i < (int) m_balls.size(); i++) {
        m_balls[i]->drawBall(painter, maxVelocity);
    }

    for (int i = 0; i < (int) m_bricks.size(); i++) {
        m_bricks[i]->drawBrick(painter);
    }
}



void Box::addBall(Ball *newBall)
{
    //Make sure that the newly placed ball doesn't go ontop of
    //another brick. Stops you from clicking on bricks to put
    //balls on top of them.
    for (int i = 0; i < (int) m_bricks.size(); i++){
        if (m_bricks[i]->isColliding(*newBall) ){
            //If lives are 0, do not bother checking if the brick
            //is overlapping, the brick isn't there.
            if (m_bricks[i]->lives() > 0){
                delete(newBall);
                return;
            }

        }
    }

    m_balls.push_back(newBall);
}


//Lay each brick in the vector so it will be placed in the window.
void Box::layBrick(Brick *newBrick)
{

    //Do not add the brick if it overlaps a ball on placement.
    for (int i = 0; i < (int) m_balls.size(); i++){
        if (newBrick->isColliding(*m_balls[i])){
            newBrick->killBrick();
            delete(newBrick);
            return;
        }
    }
    //Do not add the brick if it overlaps another brick on placement.
    for (int i = 0; i < (int) m_bricks.size(); i++){
        if ((*m_bricks[i]).isColliding(*newBrick)){
            newBrick->killBrick();
            delete(newBrick);
            return;
        }
    }
    m_bricks.push_back(newBrick);

    m_noOfBricks ++;
}




void Box::drawBox(QPainter &painter)
{
    // Draw the ellipse at it's current position.
    painter.setPen(Qt::black);
    painter.setBrush(QBrush(Qt::black));
    painter.drawRect(m_xPosition,
                     m_yPosition,
                     m_width,
                     m_height);
}

void Box::updateBalls(double time)
{
    int numOfBalls = (int) m_balls.size();

    // Calculate any reflections of the walls.
    for (int i = 0; i < numOfBalls; i++) {
        m_balls[i]->moveBall(time);

        // If out of bounds, flip x or y velocity and reflect balls position along the wall.

        // Check the West wall.
        if (m_balls[i]->getXMinimum() < getWestWall()) {
            m_balls[i]->scaleXVelocity(-1);
            m_balls[i]->setXPosition(m_balls[i]->getPosition().x()
                                 + 2 * (getWestWall() - m_balls[i]->getXMinimum()));

        }

        // Check the East wall.
        if (m_balls[i]->getXMaximum() > getEastWall()) {
            m_balls[i]->scaleXVelocity(-1);
            m_balls[i]->setXPosition(m_balls[i]->getPosition().x()
                                 + 2 * (getEastWall() - m_balls[i]->getXMaximum()));

        }

        // Check the North wall.
        if (m_balls[i]->getYMinimum() < getNorthWall()) {
            m_balls[i]->scaleYVelocity(-1);
            m_balls[i]->setYPosition(m_balls[i]->getPosition().y()
                                 + 2 * (getNorthWall() - m_balls[i]->getYMinimum()));

        }

        // Check the South wall.
        if (m_balls[i]->getYMaximum() > getSouthWall()) {
            m_balls[i]->scaleYVelocity(-1);
            m_balls[i]->setYPosition(m_balls[i]->getPosition().y()
                                 + 2 * (getSouthWall() - m_balls[i]->getYMaximum()));

        }
    }

    // Check for collision between balls.
    for (int i = 0; i < numOfBalls; i++) {
        for (int j = i + 1; j < numOfBalls; j++) {
            if (m_balls[i]->isColliding(*m_balls[j])) {
                collide(*m_balls[i], *m_balls[j]);
            }
        }
    }

    // Check for collision between bricks and balls.
    for (int i = 0; i < m_noOfBricks; i++) {
        for (int j = 0; j < numOfBalls; j++) {
            if (m_bricks[i]->lives() != 0){

                bool collision = collide(*m_bricks[i], *m_balls[j]);

            }

        }
    }



}


bool Box::collide(Brick &brick, Ball &ball){
    if (brick.isColliding(ball)){
        brick.collision(ball);
        return true;
    }
    return false;
}
std::vector<Ball *> Box::balls() const
{
    return m_balls;
}

void Box::setBalls(const std::vector<Ball *> &balls)
{
    m_balls = balls;
}

std::vector<Brick *> Box::bricks() const
{
    return m_bricks;
}

void Box::setBricks(const std::vector<Brick *> &bricks)
{
    m_bricks = bricks;
}


void Box::collide(Ball &ball1, Ball &ball2) {
    QVector2D distance = ball1.getPosition() - ball2.getPosition();

    //just added this to make the extension a little nicer before handing out code
    QVector2D velocity = ball1.getVelocity() - ball2.getVelocity();
    //check if they're moving apart or together,
    //if the dot product of their relative positions and velocities is positive
    //then they are moving apart, who are we to stop them?
    if(QVector2D::dotProduct(distance,velocity)>0){
        return;
    }

    // Get unit vector.
    distance = distance / distance.length();

    // Get scale of reflected velocity.
    double ball1dot = QVector2D::dotProduct(ball1.getVelocity(), distance);
    double ball2dot = QVector2D::dotProduct(ball2.getVelocity(), distance);

    // Apply the new veolocities to their balls.
    ball1.setVelocity(ball1.getVelocity() + (ball2dot - ball1dot) * distance);
    ball2.setVelocity(ball2.getVelocity() + (ball1dot - ball2dot) * distance);




}

double Box::getNorthWall()
{
    return m_yPosition;
}
int Box::height() const
{
    return m_height;
}

void Box::setHeight(int height)
{
    m_height = height;
}

int Box::width() const
{
    return m_width;
}

void Box::setWidth(int width)
{
    m_width = width;
}

int Box::noOfBricks() const
{
    return m_noOfBricks;
}



double Box::getEastWall()
{
    return m_xPosition + m_width;
}

double Box::getWestWall()
{
    return m_xPosition;
}

double Box::getSouthWall()
{
    return m_yPosition + m_height;
}

Box::~Box()
{
    for (int i = 0; i < (int) m_balls.size(); i++) {
        delete m_balls[i];
    }
}

Box::Box(int xPosition, int yPosition, int width, int height)
    : m_xPosition(xPosition)
    , m_yPosition(yPosition)
    , m_width(width)
    , m_height(height)
{
    m_noOfBricks = 0;
}
