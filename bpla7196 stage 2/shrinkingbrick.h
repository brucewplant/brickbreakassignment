#ifndef SHRINKINGBRICK_H
#define SHRINKINGBRICK_H

#include "brick.h"

/*
 * The class for the shrinking brick. These bricks shrink
 * when hit. Use of the bridge pattern here, as the interface of
 * brick has been decoupled from its implementation, allowing it to
 * be re-implemented here.
 */

class ShrinkingBrick: public Brick
{
public:
    using Brick::Brick;

    // Brick interface
public:
    virtual void collision(Ball &ball);
};

#endif // SHRINKINGBRICK_H
