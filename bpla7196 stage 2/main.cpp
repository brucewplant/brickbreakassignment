#include "ballinabox.h"
#include <QApplication>
#include <QRectF>
#include <iostream>
#include <fstream>
#include <sstream>


const int BORDER_SIZE = 15;

int main(int argc, char *argv[])
{
    // Default values
    int boxWidth = 640;
    int boxHeight = 360;
    int frameRate = 60;
    int ballRadius = 20;
    int ballXPosition = 100;
    int ballYPosition = 100;
    int ballVelocity = 5;



    std::vector<std::string> brickConfig;

    // Read in the contents of the config file.
    std::string line;
    std::ifstream myfile("config.txt"); // TODO: Change for the lab.
    if (myfile.is_open()) {
        while (getline(myfile,line)) {
            std::istringstream stream(line);

            std::string name;
            std::string value;

            stream >> name >> value;
            if (name == "boxWidth") {
                std::istringstream  (value) >> boxWidth;

            } else if (name == "boxHeight") {
                std::istringstream  (value) >> boxHeight;

            } else if (name == "frameRate") {
                std::istringstream  (value) >> frameRate;

            } else if (name == "ballRadius") {
                std::istringstream  (value) >> ballRadius;

            } else if (name == "ballXPosition") {
                std::istringstream  (value) >> ballXPosition;

            } else if (name == "ballYPosition") {
                std::istringstream  (value) >> ballYPosition;

            } else if (name == "ballVelocity") {
                std::istringstream  (value) >> ballVelocity;

            } else if (name == "brick") {
                brickConfig.push_back(value);
                /*
                 * Pushes back the string after brick
                 * in the config file so that it can be
                 * processed later.
                 */

            }else {
                std::cout << "Ignoring invalid config setting name: '" << name << "'\n";
            }
        }
        myfile.close();
    } else {
        std::cout << "Unable to open config file" << std::endl;
        return 1;
    }

    // Check for valid size of box and size of ball, see if it all fits.
    QRectF boxBound(0, 0, boxWidth, boxHeight);
    QRectF ballBound(ballXPosition - ballRadius,
                     ballYPosition - ballRadius,
                     2 * ballRadius,
                     2 * ballRadius);
    if (!boxBound.contains(ballBound)) {
        // Ball is not contained entirely within the box.
        std::cout << "The ball is either too large, is in the wrong position, or the box is too small.\n";
        std::cout << "Please modify the config file.\n";
        return 1;
    }

    // Check for valid config values.
    if (frameRate < 1 || ballRadius < 1 || ballVelocity < 0) {
        std::cout << "Supply only positive integer values.\n";
        std::cout << "Please modify the config file.\n";
        return 1;
    }

    // Everything's good, let's get this started...
    QApplication a(argc, argv);
    BallInABox w(boxWidth,
                 boxHeight,
                 BORDER_SIZE,
                 frameRate,
                 ballRadius,
                 ballXPosition,
                 ballYPosition,
                 ballVelocity,
                 brickConfig);
    w.show();

    return a.exec();
}



