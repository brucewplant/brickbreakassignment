#ifndef BALL_H
#define BALL_H

#include <QPainter>
#include <QVector2D>
#include <QRectF>
#include <QtCore>
#include <QColor>

class Ball
{
public:
    Ball(double xPosition,
         double yPosition,
         unsigned int radius,
         double velocity = 0);

    ~Ball() {}

    void drawBall(QPainter &painter, double maxVelocity);

    void moveBall(double time);

    QVector2D getPosition() const;
    void setXPosition(double xPosition);
    void setYPosition(double yPosition);

    QVector2D getVelocity() const;
    void setVelocity(QVector2D newVelocity);
    void scaleXVelocity(double scale);
    void scaleYVelocity(double scale);

    QRectF getBoundingRect();

    unsigned int getRadius() const;

    bool isColliding(Ball &otherBall);

    double getXMinimum() const;
    double getXMaximum() const;
    double getYMinimum() const;
    double getYMaximum() const;

private:
    QVector2D m_position;

    unsigned int m_radius;

    QVector2D m_velocity;

    QRectF m_boundingRect;
};

#endif // BALL_H
