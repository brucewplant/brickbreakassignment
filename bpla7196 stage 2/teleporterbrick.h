#ifndef TELEPORTERBRICK_H
#define TELEPORTERBRICK_H
#include "brick.h"


/*
 * The class for the teleporter brick. These teleport away the offending
 * ball when hit. Use of the bridge pattern here, as the interface of
 * brick has been decoupled from its implementation, allowing it to
 * be re-implemented here.
 */

class TeleporterBrick: public Brick
{
public:
    using Brick::Brick;



    // Brick interface
public:
    void collision(Ball &ball);

    // Brick interface
public:
    virtual std::string special();
};

#endif // TELEPORTERBRICK_H
