#ifndef BOX_H
#define BOX_H

#include "ball.h"
#include "brick.h"
#include "teleporterbrick.h"
#include <QPainter>
#include <QtGlobal>
#include <iostream>
#include <fstream>
#include <sstream>

// The world of this simulator. Using the Singleton pattern.
class Box
{
public:
    ~Box();

    void render(QPainter &painter, double time);

    void addBall(Ball *newBall);

    void layBrick(Brick *newBrick);

    void drawBox(QPainter &painter);

    void updateBalls(double time);

    static Box *getInstance(int xPosition, int yPosition, int width, int height);

    int noOfBricks() const;


    std::vector<Brick *> bricks() const;
    void setBricks(const std::vector<Brick *> &bricks);

    std::vector<Ball *> balls() const;
    void setBalls(const std::vector<Ball *> &balls);

    int width() const;
    void setWidth(int width);

    int height() const;
    void setHeight(int height);

private:
    Box();

    static Box *instance;

    Box(int xPosition,
        int yPosition,
        int width,
        int height);

    double getWestWall();
    double getEastWall();
    double getSouthWall();
    double getNorthWall();

    int m_xPosition;
    int m_yPosition;
    int m_width;
    int m_height;

    int m_noOfBricks;

    void collide(Ball &ball1, Ball &ball2);
    bool collide(Brick &brick, Ball &ball);
    void moveBrick(Brick *brick);

    std::vector<Ball *> m_balls;
    std::vector<Brick *> m_bricks;
};

#endif // BOX_H
