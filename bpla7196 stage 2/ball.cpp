#include "ball.h"

#include <iostream>

Ball::Ball(double xPosition,
           double yPosition,
           unsigned int radius,
           double velocity)
          : m_position(xPosition, yPosition)
          , m_radius(radius)
          , m_boundingRect(xPosition - (int) radius,
                           yPosition - (int) radius,
                           radius * 2,
                           radius * 2)
{
    // Get x and y components of the velocity. i.e. a random direction.
    double xVelocity = 0;
    double yVelocity = 0;

    double rand = ((double) qrand() / (RAND_MAX)); // random number [0,1]
    // get random x component.
    xVelocity = 2 * velocity * rand - velocity;

    // calculate the required y component to get the required overall velocity.
    yVelocity = qSqrt(qPow(velocity, 2) - qPow(xVelocity, 2));
    // Randomly chose a positive or negative y component.
    yVelocity = (qrand() % 2 == 0) ? yVelocity : -yVelocity;

    m_velocity.setX(xVelocity);
    m_velocity.setY(yVelocity);
}


void Ball::drawBall(QPainter &painter, double maxVelocity)
{
    painter.setPen(Qt::transparent);

    // map this balls velocity to the red - > white colour scale.
    double val = getVelocity().length() / maxVelocity;

    QColor color;

    color.setRgbF(1, 1 - val,  1 - val);

    painter.setBrush(QBrush(color));
    painter.drawEllipse(getBoundingRect());
}

void Ball::moveBall(double dt)
{
    m_position.setX(m_position.x() + dt * m_velocity.x());
    m_position.setY(m_position.y() + dt * m_velocity.y());
}

QVector2D Ball::getPosition() const
{
    return m_position;
}

void Ball::setXPosition(double xPosition)
{
    m_position.setX(xPosition);
}

void Ball::setYPosition(double yPosition)
{
    m_position.setY(yPosition);
}

QVector2D Ball::getVelocity() const
{
    return m_velocity;
}

void Ball::setVelocity(QVector2D newVelocity)
{
    m_velocity = newVelocity;
}

void Ball::scaleXVelocity(double scale)
{
    m_velocity.setX(m_velocity.x() * scale);
}

void Ball::scaleYVelocity(double scale)
{
    m_velocity.setY(m_velocity.y() * scale);
}

QRectF Ball::getBoundingRect()
{
    m_boundingRect.setRect(getXMinimum(),
                          getYMinimum(),
                          m_radius * 2,
                          m_radius * 2);
    return m_boundingRect;
}

unsigned int Ball::getRadius() const
{
    return m_radius;
}

bool Ball::isColliding(Ball &otherBall)
{
    // Check if this ball is overlapping the other ball.
    double xd = getPosition().x() - otherBall.getPosition().x();
    double yd = getPosition().y() - otherBall.getPosition().y();

    double sumRadius = getRadius() + otherBall.getRadius();
    double sqrRadius = sumRadius * sumRadius;

    double distSqr = (xd * xd) + (yd * yd);

    if (distSqr <= sqrRadius)
    {
        return true;
    }

    return false;
}

double Ball::getXMinimum() const
{
    return m_position.x() - (int) m_radius;
}

double Ball::getXMaximum() const
{
    return m_position.x() + (int) m_radius;
}

double Ball::getYMinimum() const
{
    return m_position.y() - (int) m_radius;
}

double Ball::getYMaximum() const
{
    return m_position.y() + (int) m_radius;
}
