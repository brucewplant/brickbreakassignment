#include "ballinabox.h"
#include "shrinkingbrick.h"
#include "ui_ballinabox.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include <sstream>

BallInABox::BallInABox(int boxWidth,
                       int boxHeight,
                       int borderSize,
                       int frameRate,
                       int ballRadius,
                       int ballXPosition,
                       int ballYPosition,
                       int ballVelocity,
                       std::vector<std::string> brickConfig,
                       QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::BallInABox)
    , m_box(Box::getInstance(borderSize,
                             borderSize,
                             boxWidth,
                             boxHeight))
    , m_dt(1000.0 / frameRate)
    , m_ballVelocity(ballVelocity)
    , m_ballRadius(ballRadius)
{
    // Seed the random number generator. Used in Ball class.
    qsrand((unsigned)time(NULL));

    // Create the first ball.
    m_box->addBall(new Ball(ballXPosition + borderSize,
                            ballYPosition + borderSize,
                            m_ballRadius,
                            m_ballVelocity));

    for (int i = 0; i < (int) brickConfig.size(); i++){
        std::cout << brickConfig[i] << '\n';
        std::istringstream ss(brickConfig[i]);
        std::string token;

        //Brick attributes
        int brickXPosition;
        int brickYPosition;
        int brickWidth = 100;
        int brickHeight = 50;
        int brickLives;
        int rcolor;
        int gcolor;
        int bcolor;
        std::string brickColor;

        int j = 0;
        while (std::getline(ss, token, ',')){
            /*
             * Process each argument given for the brick configuration here,
             * creating each brick and pushing it into the vector of bricks
             * for the box class later.
             */
            if (j == 0){
                std::istringstream(token) >> brickXPosition;
            }
            else if (j == 1){
                std::istringstream(token) >> brickYPosition;

            }
            else if (j == 2){
                //If the brick exists outside the border, set its lives to 0
                //so it doesn't appear. We will delete it with all the bricks
                //declared by the user (regardless of legality) later.
                if (brickXPosition < borderSize || brickYPosition < borderSize){
                    brickLives = 0;
                }else{
                    std::istringstream(token) >> brickLives;
                }


                std::cout << brickLives << "livesno" << '\n';
            }
            else if(j == 3){
                std::istringstream(token) >> brickWidth;

            }else if(j == 4){
                std::istringstream(token) >> brickHeight;
            }else if(j == 5){
                std::istringstream(token) >> rcolor; //Colour components of bricks
            }else if(j == 6){
                std::istringstream(token) >> gcolor;
            }else if(j == 7){
                std::istringstream(token) >> bcolor;
            }
            else if (j == 8){

                /*
                 * These create the bricks depending on their type.
                 * They will be treated identically by the box class,
                 * as their interfaces are identical, but the implementation
                 * is different.
                 */
                if (token == "normal"){
                    m_box->layBrick(new Brick(brickXPosition,
                                              brickYPosition,
                                              brickWidth,
                                              brickHeight,
                                              brickLives,
                                              boxWidth,
                                              boxHeight,
                                              rcolor,
                                              gcolor,
                                              bcolor));
                }else if (token == "teleport"){
                    m_box->layBrick(new TeleporterBrick(brickXPosition,
                                                        brickYPosition,
                                                        brickWidth,
                                                        brickHeight,
                                                        brickLives,
                                                        boxWidth,
                                                        boxHeight,
                                                        rcolor,
                                                        gcolor,
                                                        bcolor));
                }else if (token == "shrink"){
                    m_box->layBrick(new ShrinkingBrick(brickXPosition,
                                                        brickYPosition,
                                                        brickWidth,
                                                        brickHeight,
                                                        brickLives,
                                                        boxWidth,
                                                        boxHeight,
                                                       rcolor,
                                                       gcolor,
                                                       bcolor));
                }
            }
            j++;
        }




        std::cout << m_box->noOfBricks() << '\n';
    }



    // Setup the window and timer.
    ui->setupUi(this);
    this->resize(boxWidth + 2 * borderSize, boxHeight + 2 * borderSize);
    this->setFixedSize(this->size()); // Fix the window size.
    this->setStyleSheet("background-color: #2f2f2f;"); // Grey border.
    this->update();
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
    m_timer->start(m_dt);

    // TODO: Test in lab, remove if faulty.
    // Start playing music...
//    m_song = new QSound("song.wav");
//    m_song->setLoops(QSound::Infinite);
//    m_song->play();
}

void BallInABox::nextFrame()
{
    update();
}

void BallInABox::mousePressEvent(QMouseEvent *event)
{
    m_box->addBall(new Ball(event->x(), event->y(), m_ballRadius, m_ballVelocity));
}

void BallInABox::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    // Turn on antialiasing for drawing. Make it look pretty.
    painter.setRenderHint(QPainter::Antialiasing);

    m_box->render(painter, m_dt / 1000);
}
Box *BallInABox::box() const
{
    return m_box;
}

void BallInABox::setBox(Box *box)
{
    m_box = box;
}


BallInABox::~BallInABox()
{
    delete m_box;
    delete ui;
    delete m_timer;
    //    m_song->stop();
//    delete m_song;
}
