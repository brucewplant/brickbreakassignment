#ifndef BRICK_H
#define BRICK_H

#include <QPainter>
#include <QVector2D>
#include <QRectF>
#include <QtCore>
#include <QColor>
#include "ball.h"


#include <iostream>
#include <fstream>
#include <sstream>

/*
 * The class for the basic brick. It is implemented
 * for every brick type.
 */

class Brick
{
public:
    Brick();
    Brick(unsigned int, unsigned int, unsigned int,
          unsigned int, unsigned int, int, int, int, int, int);
    virtual ~Brick();

    void drawBrick(QPainter &painter);
    QRectF getBoundingRect();

    QVector2D position() const;
    void setPosition(const QVector2D &position);

    unsigned int height() const;
    void setHeight(unsigned int height);

    unsigned int width() const;
    void setWidth(unsigned int width);

    unsigned int lives() const;
    void loseLife();
    void killBrick();

    bool isColliding(Ball &ball);
    bool isColliding(Brick &brick);
    virtual void collision(Ball &ball);
//    void reflectBall(Ball &ball);
    virtual std::string special(); //Contains any special methods for the bridged classes.
    //Returns string, can be edited as needed by future users. Currently it is unnecessary
    //(i.e. it no longer does anything).

protected:
    QVector2D m_position;

    unsigned int m_height;
    unsigned int m_width;

    unsigned int m_lives;

    QRectF m_boundingRect;

    QColor m_color;

    int m_bwidth;
    int m_bheight;




};

#endif // BRICK_H
