#-------------------------------------------------
#
# Project created by QtCreator 2014-03-19T11:18:19
#
#-------------------------------------------------

QT      += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = Assignment1
TEMPLATE = app


SOURCES += main.cpp\
        ballinabox.cpp \
    ball.cpp \
    box.cpp \
    brick.cpp \
    teleporterbrick.cpp \
    shrinkingbrick.cpp

HEADERS  += ballinabox.h \
    ball.h \
    box.h \
    brick.h \
    teleporterbrick.h \
    shrinkingbrick.h

FORMS    += ballinabox.ui

OTHER_FILES += \
    bounce.caf \
    pop.wav \
    pop2.wav \
    song.wav
