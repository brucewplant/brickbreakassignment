#ifndef BALLINABOX_H
#define BALLINABOX_H


#include <QDialog>
#include <QPainter>
#include <QSound>
#include <QMouseEvent>
#include <time.h>
#include "box.h"

namespace Ui {

static const int MAX_NUMBER_BALLS = 4;

class BallInABox;
}

class BallInABox : public QDialog
{
    Q_OBJECT

public:
    explicit BallInABox(int boxWidth,
                        int boxHeight,
                        int borderSize,
                        int frameRate,
                        int ballRadius,
                        int ballXPosition,
                        int ballYPosition,
                        int ballVelocity,
                        std::vector<std::string> brickConfig,
                        QWidget *parent = 0);
    ~BallInABox();

    void mousePressEvent(QMouseEvent *event);

    Box *box() const;
    void setBox(Box *box);

public slots:
    void nextFrame();

protected:
    void paintEvent(QPaintEvent *event);

private:
    Ui::BallInABox *ui;
    Box *m_box;
    double m_dt;
    double m_ballVelocity;
    unsigned int m_ballRadius;

    QTimer *m_timer;

    QSound *m_song;
};

#endif // BALLINABOX_H
