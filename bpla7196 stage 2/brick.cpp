#include "brick.h"
#include "ballinabox.h"
#include "box.h"

Brick::Brick(unsigned int xpos, unsigned int ypos, unsigned int width, unsigned int height,
             unsigned int lives, int boxWidth, int boxHeight, int rcolor, int gcolor, int bcolor)
    : m_lives(lives)
    , m_width(width)
    , m_height(height)
    , m_bwidth(boxWidth)
    , m_bheight(boxHeight)

{

    m_position.setX(xpos);
    m_position.setY(ypos);
//    m_width = 100;
//    m_height = 50;

    m_color.setRed(rcolor);
    m_color.setGreen(gcolor);
    m_color.setBlue(bcolor);
//    if (color == "blue"){
//        m_color.setRgbF(0,0,1);
//    }else if (color == "green"){
//        m_color.setRgbF(0,1,0);
//    }else{
//        m_color.setRgbF(1,0,0);
//    }
}

Brick::~Brick()
{

}

void Brick::drawBrick(QPainter &painter)
{
    //Don't draw the brick if it has no lives left.
    if (m_lives > 0){
        painter.setPen(Qt::white);

        painter.setBrush(QBrush(m_color));
        painter.drawRect(getBoundingRect());
    }

}

QRectF Brick::getBoundingRect()
{
    m_boundingRect.setRect(m_position.x(),
                          m_position.y(),
                          m_width,
                          m_height);
    return m_boundingRect;
}

QVector2D Brick::position() const
{
    return m_position;
}

void Brick::setPosition(const QVector2D &position)
{
    m_position = position;
}
unsigned int Brick::height() const
{
    return m_height;
}

void Brick::setHeight(unsigned int height)
{
    m_height = height;
}
unsigned int Brick::width() const
{
    return m_width;
}

void Brick::setWidth(unsigned int width)
{
    m_width = width;
}
unsigned int Brick::lives() const
{
    return m_lives;
}

void Brick::loseLife()
{
    //The color of the brick darkens as it loses lives. Each component is
    //reduced to half of its value.
    m_color.setRgbF(m_color.redF()*.5, m_color.greenF()*.5, m_color.blueF()*.5);
    m_lives --;
}

void Brick::killBrick()
{
    m_lives = 0;
}

bool isAboveLine(int x, int y, int oX, int oY, Ball &ball)
{
    return (((oX - x) * (ball.getPosition().y() - y )) - ((oY - y) * (ball.getPosition().x() - x)) ) > 0;
}

bool Brick::isColliding(Ball &ball){

     if (ball.getBoundingRect().intersects(m_boundingRect)){
        return true;

     }
     return false;
}


bool Brick::isColliding(Brick &brick)
{

    if (m_lives < 1){
        return false;
    }
    if (brick.getBoundingRect().intersects(m_boundingRect)){
        std::cout << "theres a colision" << '\n';
        return true;
    }
    std::cout << brick.getBoundingRect().intersects(m_boundingRect) << '\n';
    std::cout << m_boundingRect.intersects(brick.getBoundingRect()) << '\n';
    return false;
}


/*
 * Works as such: imagine if you were to draw two lines from corner to corner
 * on the brick. You then determine where the ball is in relation to these lines.
 * If it is above both, the ball hit the top (assuming a collision). If it is
 * above the AD line only, it has struck the right. Above the BC it has struck
 * the left. If it is below both lines, the ball has hit the bottom side of the
 * brick.
 */
void Brick::collision(Ball &ball)
{
    if (isColliding(ball)){

        int topLeftX = m_position.x();
        int topLeftY = m_position.y();

        int topRightX = topLeftX + m_width;
        int topRightY = topLeftY;

        int botLeftX = topLeftX;
        int botLeftY = topLeftY + m_height;

        int botRightX = topRightX;
        int botRightY = botLeftY;


            bool isAboveAD = isAboveLine(botRightX, botRightY, topLeftX, topLeftY, ball);
            bool isAboveBC = isAboveLine(topRightX, topRightY, botLeftX, botLeftY, ball);

            loseLife();

            if (m_lives < 1){
                std::cout << "lost life, have " << m_lives << " left\n";
                return;
            }

            if (isAboveAD)
            {
                if (isAboveBC)
                {
                    //TOP COLLISION
                    ball.scaleYVelocity(-1);
                    return;
                }
                else
                {
                    //RIGHT COLLISION
                    ball.scaleXVelocity(-1);
                    return;
                }
            }
            else
            {
                if (isAboveBC)
                {
                    //LEFT COLLISION
                    ball.scaleXVelocity(-1);
                    return;
                }else
                {
                    //BOTTOM COLLISION
                    ball.scaleYVelocity(-1);
                    return;
                }

            }
    }

    return;

}

std::string Brick::special()
{
    return "null";
}





