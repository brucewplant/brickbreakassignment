#-------------------------------------------------
#
# Project created by QtCreator 2014-03-30T12:27:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = assignment1
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    coordinate.cpp \
    ball.cpp

HEADERS  += dialog.h \
    coordinate.h \
    ball.h

FORMS    += dialog.ui
