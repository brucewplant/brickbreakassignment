#include "coordinate.h"

Coordinate::Coordinate()
{
}

Coordinate::Coordinate(unsigned int xCoordinate,
                       unsigned int yCoordinate,
                       unsigned int frameWidth,
                       unsigned int frameHeight)
    : m_xCoordinate(xCoordinate)
    , m_yCoordinate(yCoordinate)
    , m_frameWidth(frameWidth)
    , m_frameHeight(frameHeight)
{

}

unsigned int Coordinate::xCoordinate() const
{
    return m_xCoordinate;
}

void Coordinate::setXCoordinate(unsigned int xCoordinate)
{
    m_xCoordinate = xCoordinate;
}
unsigned int Coordinate::yCoordinate() const
{
    return m_yCoordinate;
}

void Coordinate::setYCoordinate(unsigned int yCoordinate)
{
    m_yCoordinate = yCoordinate;
}
unsigned int Coordinate::frameWidth() const
{
    return m_frameWidth;
}

void Coordinate::setFrameWidth(unsigned int frameWidth)
{
    m_frameWidth = frameWidth;
}
unsigned int Coordinate::frameHeight() const
{
    return m_frameHeight;
}

void Coordinate::setFrameHeight(unsigned int frameHeight)
{
    m_frameHeight = frameHeight;
}

void Coordinate::changeInXCoordinate(int change){
    m_xCoordinate += change;
}

void Coordinate::changeInYCoordinate(int change){
    m_yCoordinate += change;
}


