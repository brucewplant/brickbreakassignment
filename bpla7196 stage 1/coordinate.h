#ifndef COORDINATE_H
#define COORDINATE_H

#include <stdlib.h>



class Coordinate
{
public:
    Coordinate();


    Coordinate(unsigned
        int xCoordinate,
        unsigned int yCoordinate,
        unsigned int frameWidth,
        unsigned int frameHeight);

    void changeInXCoordinate(int change);
    void changeInYCoordinate(int change);

    unsigned int xCoordinate() const;
    void setXCoordinate(unsigned int xCoordinate);

    unsigned int yCoordinate() const;
    void setYCoordinate(unsigned int yCoordinate);

    unsigned int frameWidth() const;
    void setFrameWidth(unsigned int frameWidth);

    unsigned int frameHeight() const;
    void setFrameHeight(unsigned int frameHeight);

private:
    unsigned int m_xCoordinate;
    unsigned int m_yCoordinate;
    unsigned int m_frameWidth;
    unsigned int m_frameHeight;
};

#endif // COORDINATE_H
