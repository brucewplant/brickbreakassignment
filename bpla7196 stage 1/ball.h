#ifndef BALL_H
#define BALL_H

#include "coordinate.h"
#include <QPainter>

class Ball
{

public:
    Ball(Coordinate coordinate);

    Ball(Coordinate coordinate,
        unsigned int radius);
    Ball(Coordinate coordinate,
        unsigned int radius,
        double xVelocity,
        double yVelocity,
        const char* color);

    Ball();
    ~Ball();

    void render(QPainter &painter, unsigned int time);
    bool isXCollision();
    bool isYCollision();


    unsigned int radius() const;
    void setRadius(unsigned int radius);

    double xVelocity() const;
    void setXVelocity(double xVelocity);

    double yVelocity() const;
    void setYVelocity(double yVelocity);

    Coordinate coordinate() const;
    void setCoordinate(const Coordinate &coordinate);

    bool paused;

    const char *color();
    void setColor(const char *color);



private:


    Coordinate m_coordinate;
    unsigned int m_radius;
    double m_xVelocity;
    double m_yVelocity;
    const char* m_color;

};

#endif // BALL_H
