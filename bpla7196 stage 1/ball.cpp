#include "ball.h"



Ball::Ball()
{
}

Ball::~Ball(){

}

Ball::Ball(Coordinate coordinate, unsigned int radius, double xVelocity, double yVelocity, const char *color){
    setCoordinate(coordinate);
    setRadius(radius);
    setXVelocity(xVelocity);
    setYVelocity(yVelocity);
    setColor(color);
    paused = false;

}

void Ball::render(QPainter &painter, unsigned int time){
    //Detect collisions for horizontal and vertical borders.

    if (paused) {

    }else {
        if(isXCollision()){
            m_xVelocity *= -1;
        }
        if(isYCollision()){
            m_yVelocity *= -1;
        }

        m_coordinate.changeInXCoordinate(m_xVelocity);
        m_coordinate.changeInYCoordinate(m_yVelocity);
    }

    painter.setPen ( Qt::black );

    //This has to be a hex value in the config value, starting with "#", exactly 6 digits.
    painter.setBrush( QBrush( m_color ) );
    painter.drawEllipse(m_coordinate.xCoordinate() - (int) m_radius,
        m_coordinate.yCoordinate() -(int) m_radius,
        m_radius * 2,
        m_radius * 2);
}

/*
 * The following 2 functions won't stop a ball spawned outside
 * the window from existing there, and will prevent them from
 * fully entering (or indeed exiting if they manage to reach
 * the border). They will just bounce along the edge, half in,
 * half out. Checking for this error is done in dialog.cpp.
 */
bool Ball::isXCollision(){
    return (m_coordinate.xCoordinate() <
            (signed int) (m_radius))
        ||  (m_coordinate.xCoordinate() >
            (signed int) (m_coordinate.frameWidth() - (signed int) m_radius));
}

bool Ball::isYCollision(){
    return  (m_coordinate.yCoordinate() >
            (signed int) (m_coordinate.frameHeight() - (signed int) m_radius))
        ||  (m_coordinate.yCoordinate() <
            (signed int) (m_radius));
}

double Ball::yVelocity() const
{
    return m_yVelocity;
}

Coordinate Ball::coordinate() const
{
    return m_coordinate;
}


void Ball::setCoordinate(const Coordinate &coordinate)
{
    m_coordinate = coordinate;
}
const char *Ball::color()
{
    return m_color;
}

void Ball::setColor(const char *color)
{
    m_color = color;
}

void Ball::setYVelocity(double yVelocity)
{
    m_yVelocity = yVelocity;
}

double Ball::xVelocity() const
{
    return m_xVelocity;
}

void Ball::setXVelocity(double xVelocity)
{
    m_xVelocity = xVelocity;
}

unsigned int Ball::radius() const
{
    return m_radius;
}

void Ball::setRadius(unsigned int radius)
{
    m_radius = radius;
}

