#include "dialog.h"
#include "ui_dialog.h"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>

using namespace std;

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{

    /* Read the file and store the ball's values. */
    std::ifstream file("config.txt");

    std::string str;

    // The following 8 values are the attributes of the ball and box.
    // Default values are put here, if the user doesn't wish to enter
    // them.
    char* color = new char[8]; //8 because #234567'\0'
    double xVelocity = 20.0;
    double yVelocity = 20.0;
    unsigned int radius = 20;
    unsigned int xCoordinate = 100;
    unsigned int yCoordinate = 100;
    unsigned int frame_width = 640;
    unsigned int frame_height = 480;

    stringstream strvalue;

    int colorlength = 0;    // Used to make sure the hex value isn't too long.

    //This loop reads the file, locates each value by name
    //and associates it with the correct identifier.
    while (std::getline(file, str))
    {
        int loc = str.find("=");
        std::string attr_name = str.substr(0, loc);
        std::string attr = str.substr(loc + 1);
        strvalue << attr;
        if (attr_name == "ballcolor"){
            colorlength = attr.size();
//            cout << attr.c_str() << '\n';
            std::copy(attr.begin(), attr.end(), color);
            color[7] = '\0';
//            cout << color << '\n';
        }else if (attr_name  == ("ballxvelocity")){
            xVelocity = (atof(attr.c_str()));
        }else if (attr_name  == ("ballyvelocity")){
            yVelocity = (atof(attr.c_str()));
        }else if (attr_name == ("ballradius")){
            strvalue >> radius;

            radius = ((atof(attr.c_str())));
        }else if (attr_name  == ("ballxposition")){
            xCoordinate = ((atof(attr.c_str())));
        }else if (attr_name  == ("ballyposition")){
            yCoordinate = (atof(attr.c_str()));
        }else if (attr_name  == ("boxhorizontal")){
            frame_width = (atoi(attr.c_str()));
        }else if (attr_name  == ("boxvertical")){
            frame_height = (atoi(attr.c_str()));
        }
    }

    /************* Error checking here ****************/

    /* Make sure the ball starts inside the box! */
    /*
     * These are all cast as signed ints because if the ball is too
     * far to the left or top it has negative coordinates not recognised
     * by the program. HOWEVER, in no other place in the program are
     * signed ints needed, so there is no need to make them as such permanently.
     */
    if ((signed int) xCoordinate - (signed int) radius < 0
     || (signed int) yCoordinate - (signed int) radius < 0
     || (signed int) xCoordinate + (signed int) radius > (signed int) frame_width
     || (signed int) yCoordinate + (signed int) radius > (signed int) frame_height){
        cout << "The ball exists outside the box" << '\n';
        exit(1);
    }

    /* Make sure the ball isnt too big for the box! */
    if (radius*2 > frame_width || radius*2 > frame_height){
        cout << "The ball is too big for the box" << '\n';
        exit(1);
    }

    /* Make sure the ball's colour is a hex value (and < 7 bits)! */
    if (!(check_hex_value(color, colorlength))){
        exit(1);
    }


    /************* Error checking finished ************/

    //Make the ball, set up the counter and start the ui.
    m_ball = Ball(Coordinate(xCoordinate, yCoordinate, frame_width, frame_height),
                  radius, xVelocity, yVelocity, color);
    m_counter = 0;
    ui->setupUi(this);
    this->resize(frame_width, frame_height);

    /* The following lines deal with the QTimer */
    paused = false;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
    timer->start(32);
}



void Dialog::nextFrame()
{
    update();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    m_ball.render(painter, m_counter);
    m_counter++;
}


void Dialog::on_pause_button_clicked()
{
    if(paused){
//       timer->start();
       m_ball.paused = false;
       paused = false;
    }else{
//        timer->stop();
        m_ball.paused = true;
        paused = true;
    }

}

void Dialog::on_color_button_clicked()
{
    std::string in_string = ui->color_form->text().toStdString().c_str();
    int len = in_string.size();
    char * out_string = new char[8];
    std::copy(in_string.begin(), in_string.end(), out_string);
    out_string[7] = '\0';
    if (!(check_hex_value(out_string, len))){
        ui->color_form->setText("INVALID COLOR");
    }else{
        cout << "about to begin" << '\n';
        cout << out_string << '\n';

        m_ball.setColor(out_string );
    }


}

bool Dialog::check_hex_value(char* in_val, int len){
    if (in_val[0] != '#'){
        cout << "No leading hash!" << '\n';
        return false;
    }
    if (len != 7){
        cout << "The hex color value is too large or too small!" << '\n';
        return false;
    }
    for (int i = 1; i < len; i += 1){
        if (!isxdigit(in_val[i])){
            cout << "The ball's colour is not hex!" << '\n';
            return false;
        }
    }
    return true;
}

bool Dialog::isInt(const std::string & s)
{
    //Checks first char to make sure we're dealing with an int and to take
    //account of signs.
   if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))){
        return false;
   }
   char * pointer ;
   //Attempts to conver the number to a base 10 integer.
   strtol(s.c_str(), &pointer, 10) ;

   //Return if successful.
   return (*pointer == 0) ;
}

void Dialog::on_frame_button_clicked()
{
    std::string in_string = ui->frame_form->text().toStdString();
    if (in_string == "0"){
        ui->frame_form->setText("ZERO FPS IMPOSSIBLE");
        return;
    }
    if (isInt(in_string)){
        timer->stop();
        timer->start(1000/atoi(in_string.c_str()));
    }else{
        ui->frame_form->setText("NOT AN INT");
    }

}
