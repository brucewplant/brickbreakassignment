#ifndef DIALOG_H
#define DIALOG_H
#include <QTimer>
#include "ball.h"
#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

public slots:
    void nextFrame();

protected:
    void paintEvent(QPaintEvent *event);
    bool check_hex_value(char* in_val, int len);
    bool isInt(const std::string & s);

private slots:
    void on_pause_button_clicked();

    void on_color_button_clicked();

    void on_frame_button_clicked();

private:
    bool paused;
    QTimer *timer;
    Ui::Dialog *ui;
    Ball m_ball;
    Coordinate m_coordinate;
    int m_counter;

};

#endif // DIALOG_H
