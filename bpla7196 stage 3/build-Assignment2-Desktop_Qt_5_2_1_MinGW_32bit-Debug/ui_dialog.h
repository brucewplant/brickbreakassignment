/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QLabel *ScoreLabel;
    QLabel *LivesLabel;
    QLabel *Scoreboard;
    QLabel *LivesBoard;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QStringLiteral("Dialog"));
        Dialog->resize(400, 300);
        ScoreLabel = new QLabel(Dialog);
        ScoreLabel->setObjectName(QStringLiteral("ScoreLabel"));
        ScoreLabel->setGeometry(QRect(10, 10, 61, 31));
        QFont font;
        font.setPointSize(16);
        ScoreLabel->setFont(font);
        LivesLabel = new QLabel(Dialog);
        LivesLabel->setObjectName(QStringLiteral("LivesLabel"));
        LivesLabel->setGeometry(QRect(10, 40, 91, 31));
        LivesLabel->setFont(font);
        Scoreboard = new QLabel(Dialog);
        Scoreboard->setObjectName(QStringLiteral("Scoreboard"));
        Scoreboard->setGeometry(QRect(100, 10, 61, 31));
        Scoreboard->setFont(font);
        Scoreboard->setMouseTracking(true);
        Scoreboard->setAutoFillBackground(false);
        LivesBoard = new QLabel(Dialog);
        LivesBoard->setObjectName(QStringLiteral("LivesBoard"));
        LivesBoard->setGeometry(QRect(100, 40, 91, 31));
        LivesBoard->setFont(font);
        LivesLabel->raise();
        Scoreboard->raise();
        LivesBoard->raise();
        ScoreLabel->raise();

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog", 0));
        ScoreLabel->setText(QApplication::translate("Dialog", "Score", 0));
        LivesLabel->setText(QApplication::translate("Dialog", "LivesLeft", 0));
        Scoreboard->setText(QApplication::translate("Dialog", "<html><head/><body><p><span style=\" color:#ffffff;\">0</span></p></body></html>", 0));
        LivesBoard->setText(QApplication::translate("Dialog", "0", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
