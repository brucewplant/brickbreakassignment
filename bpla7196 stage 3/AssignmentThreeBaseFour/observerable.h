#ifndef OBSERVERABLE_H
#define OBSERVERABLE_H
#include "observer.h"
#include <vector>
#include <map>
/*!
 * \brief The Observerable class
 *
 * Abstract implentation of Observerable for the Observer
 * pattern.
 *
 * Contains abstract methods for attatch observer, detatch
 * obesrver and notify.
 *
 * Detatch does nothing at the moment, as it is never used,
 * and its implementation was tricky, or at least too tricky
 * to be worthwhile, considering it's never used.
 */
class Observerable
{
public:
    virtual void attatch(Observer &observer) = 0;
    virtual void detatch(Observer &observer) = 0;
    virtual void notify() = 0;
protected:
    std::vector<Observer *> observers;

};

#endif // OBSERVERABLE_H
