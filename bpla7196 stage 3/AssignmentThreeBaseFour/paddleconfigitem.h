#ifndef PADDLECONFIGITEM_H
#define PADDLECONFIGITEM_H
#include "blockconfigitem.h"

/*!
 * \brief The PaddleConfigItem class
 *
 * PaddleConfigItem is nearly identical to BlockConfigItem.
 * The only difference is that the getItemType() method has changed,
 * to return PADDLE instead of BLOCK.
 */

class PaddleConfigItem : public BlockConfigItem
{
public:

    PaddleConfigItem();

    // ConfigItem interface
public:
    virtual Item getItemType() const;
};

#endif // PADDLECONFIGITEM_H
