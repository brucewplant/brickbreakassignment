#include "blocksobservable.h"

int BlocksObservable::blocksLeft = 0;
BlocksObservable *BlocksObservable::instance = 0;


BlocksObservable *BlocksObservable::getInstance(int blocksLeft)
{
    if (instance == 0){
        instance = new Lives(numLives);
    }
    return instance;
}

BlocksObservable::BlocksObservable::BlocksObservable(int numBlocks)
{
    blocksLeft = numBlocks;
}

void BlocksObservable::attatch(Observer &observer)
{
    observers.push_back(&observer);
}

void BlocksObservable::detatch(Observer &observer)
{
//    for (int i = 0; i < observers.size(); i++){
//        if (*observers[i] == observer){
//            observers.erase(observers.begin() + i);
//        }
//    }
}

void BlocksObservable::notify()
{
    for (int i = 0; i < observers.size(); i++){
        observers[i]->update(lives);
    }
}

void BlocksObservable::decrementBricks()
{
    blocksLeft --;
    notify();
}

void BlocksObservable::incrementBricks()
{
    blocksLeft ++;
    notify();
}


