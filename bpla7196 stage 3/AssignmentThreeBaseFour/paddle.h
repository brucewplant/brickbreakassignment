#ifndef PADDLE_H
#define PADDLE_H
#include "block.h"

/*!
 * \brief The Paddle class
 * Paddle is inherited from brick, and shares most of its methods.
 * The new methods are for moving the brick, and for making the paddle
 * a singleton.
 *
 * The move methods do not move the brick itself. Instead, they flip
 * the left or right boolean, which tells the brick it can move.
 *
 * It is the dialog class which moves the brick, it contains  the timers
 * to do this. It will move the brick left if m_left is true, and right
 * if m_right is true.
 *
 * 0 is used to represent m_paddle being un-initialised.
 */

class Paddle : public Block
{
public:

    static Paddle* getPaddle(PaddleConfigItem *config);

    virtual void decrementLives();
    void move_left(bool move);
    void move_right(bool move);
    bool left();
    bool right();
    // Block interface

public slots:
    void move();

private:

    Paddle(PaddleConfigItem *config)
        :Block(config){
    }


    static Paddle *m_paddle;
    static bool m_left;
    static bool m_right;
};

#endif // PADDLE_H
