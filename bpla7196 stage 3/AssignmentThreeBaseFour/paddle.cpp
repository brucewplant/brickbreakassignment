#include "paddle.h"



Paddle *Paddle::m_paddle = 0;
bool Paddle::m_left = false;
bool Paddle::m_right = false;


Paddle *Paddle::getPaddle(PaddleConfigItem *config)
{
    if (m_paddle == 0){
        m_paddle = new Paddle(config);

    }

    return m_paddle;
}

void Paddle::decrementLives()
{

    return;
}

void Paddle::move_left(bool move)
{
    m_left = move;
}

void Paddle::move_right(bool move)
{
    m_right = move;
}

bool Paddle::left(){
    return m_left;
}

bool Paddle::right(){
    return m_right;
}
