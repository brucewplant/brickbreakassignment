#include "dialog.h"
#include "config.h"
#include "defaults.h"
#include <QTimer>
#include <QApplication>

bool testHeight (Config *testConfig){
    if (testConfig->getHeight() == Defaults::BOX_HEIGHT)
        return true;
    return false;
}

bool testWidth (Config *testConfig){
    if (testConfig->getHeight() == Defaults::BOX_WIDTH)
        return true;
    return false;
}


bool testLives (Config *testConfig){
    if (testConfig->getHeight() == Defaults::LIVES)
        return true;
    return false;
}

bool testFinalObject (Config *testConfig){
    if (&testConfig[testConfig->size()-1]){
        return true;
    }
    return false;
}

int main(int argc, char *argv[])
{
    Config *config = new Config();
    //Unit tests
    std::cout << "UNIT TESTS:" << std::endl;
    std::cout << "TEST HEIGHT FROM CONFIG: " << testHeight(config) << std::endl;
    std::cout << "TEST WIDTH FROM CONFIG: " << testWidth(config) << std::endl;
    std::cout << "TEST LIVES FROM CONFIG: " << testLives(config) << std::endl;
    std::cout << "TEST FINAL OBJECT FROM CONFIG: " << testFinalObject(config) << std::endl;
    QApplication a(argc, argv);
    Dialog w(config);

    w.show();

//    QTimer timer;
//    QObject::connect(&timer, SIGNAL(timeout()), &w, SLOT(nextFrame()));
//    QObject::connect(&timer, SIGNAL(timeout()), w.getScene(), SLOT(advance()));
//    timer.start(1000 / 120);




    delete config;
    return a.exec();
}




