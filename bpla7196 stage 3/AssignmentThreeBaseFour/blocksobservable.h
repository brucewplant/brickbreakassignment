#ifndef BLOCKSOBSERVABLE_H
#define BLOCKSOBSERVABLE_H
#include "observerable.h"

class BlocksObservable : public Observerable
{
public:
    static BlocksObservable* getInstance(int lives);

    BlocksObservable(int numBlocks);
    // Observerable interface
    void attatch(Observer &observer);
    void detatch(Observer &observer);
    void notify();
    void decrementBricks();

    void incrementBricks();
private:
    static int blocksLeft;
    static BlocksObservable* instance;
};


#endif // BLOCKSOBSERVABLE_H
