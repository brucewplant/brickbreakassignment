#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QTimer>
#include <vector>
#include <sstream>
#include <utility>
#include "config.h"
#include "ball.h"
#include "itemfactory.h"
#include "item.h"
#include "paddle.h"
#include "observer.h"
#include "lives.h"
#include "defaults.h"
#include "blocksobservable.h"
#include <QSound>


namespace Ui
{
    class Dialog;
}
/*!
 * \brief The Dialog class
 * EDIT FOR STAGE 3: In adition to the previous comments,
 * changes to dialog include:
 *  the inclusion of keypress and kerelease events.
 *  update
 *  inheriting Observer
 *
 * Observer is inherited along with QDialog so that dialog
 * can benefit from the observer pattern, observing the "lives"
 * class.
 */

class Dialog : public QDialog, public Observer
{
    Q_OBJECT

public:
    explicit Dialog(Config *config,
                    QWidget *parent = 0);

    ~Dialog();

    QGraphicsScene * getScene();

    void mousePressEvent(QMouseEvent *event);

    /*!
     * \brief keyPressEvent
     * \param event
     * keyPressEvent(); will trigger if a key is pressed.
     * It only works for A and S keys. These will move the paddle
     * to the left or right. If you try to move beyond this, a
     * warning sound triggers and you are blocked.
     *
     * KeyPress will flip the respective movement booleans true
     * for the paddle class.
     */
    void keyPressEvent(QKeyEvent *event);
    /*!
     * \brief keyReleaseEvent
     * \param event
     *
     * Same as above, except keyrelease flips the booleans false.
     */
    void keyReleaseEvent(QKeyEvent *event);

    virtual void update(int livesLeft, int numBlocks);
    std::string intToString(int input);
public slots:
    void nextFrame();

private:
    Ui::Dialog *ui;
    QGraphicsView *view;
    QGraphicsScene *currScene;
//    QGraphicsScene *scene;
    int currLevel;
    std::vector<QGraphicsScene *> levels;
    Config *config;
    Lives *lives;
    int zCount;
    int level;
    int points;
    int boxflag;
    int numLevels;
    std::vector<int> numBlocks;
    Paddle *m_paddle;
    bool gameStart;
    QTimer timer;
    QTimer paddleTimer;

};

#endif // DIALOG_H
