#include "levelconfigitem.h"

LevelConfigItem::LevelConfigItem()
    : ConfigItem()
{
}

LevelConfigItem::~LevelConfigItem()
{

}

bool LevelConfigItem::validate(int height, int width) const
{
    return true;
}

Item LevelConfigItem::getItemType() const
{
    return LEVEL;
}

void LevelConfigItem::addParameter(std::string name, double value)
{
    return;
}
