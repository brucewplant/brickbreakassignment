#include "lives.h"

int Lives::lives = 0;
int Lives::numBricks = 0;
Lives *Lives::instance = 0;


Lives *Lives::getInstance(int numLives)
{
    if (instance == 0){
        instance = new Lives(numLives);
    }
    return instance;
}

Lives::Lives::Lives(int numLives)
{
    lives = numLives;
}

void Lives::attatch(Observer &observer)
{
    observers.push_back(&observer);
}

void Lives::detatch(Observer &observer)
{
//    for (int i = 0; i < observers.size(); i++){
//        if (*observers[i] == observer){
//            observers.erase(observers.begin() + i);
//        }
//    }
}

void Lives::notify()
{
    for (int i = 0; i < observers.size(); i++){
        observers[i]->update(lives, numBricks);
    }
}

void Lives::decrementLives()
{
    QSound::play("sounds/Windows XP Battery Critical.wav");
    lives --;
    notify();
}

void Lives::incrementBricks()
{
    numBricks ++;
    notify();
}

void Lives::decrementBricks()
{
    numBricks --;
    notify();
}

