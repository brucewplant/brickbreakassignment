#include "dialog.h"
#include "ui_dialog.h"


Dialog::Dialog(Config *config,
               QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
    , config(config)
    , zCount(0)
    , points(0)
    , boxflag(0)
    , currLevel(0)
    , gameStart(false)
{

    Lives *lives = Lives::getInstance(config->getLives());
    lives->attatch(*this);
    ui->setupUi(this);
    this->resize(config->getWidth(), config->getHeight());
    this->setFixedSize(this->size());
    QGraphicsScene *scene = new QGraphicsScene(this);
//    scene = new QGraphicsScene(this);
//    sceneVector->push_back(scene);
    scene->setSceneRect(0, 0, config->getWidth(), config->getHeight());
    scene->setBackgroundBrush(Defaults::BOX_COLOR);
    levels.push_back(scene);

    view = new QGraphicsView(this);
    view->setScene(scene);
    view->setRenderHint(QPainter::Antialiasing);

    numLevels = 0;
    bool firstLevel = false;
    numBlocks.push_back(0);
    for (size_t i = 0; i < config->size(); i++) {
        zCount++;
        if ((*config)[i]->getItemType() == LEVEL) {
            if (!firstLevel){
                firstLevel = true;
                continue;
                //To work with the old config file, we need to ignore
                //The first instance of [LEVEL]
            }
            numBlocks.push_back(0);
            numLevels ++;

            scene = new QGraphicsScene(this);
            scene->setSceneRect(0, 0, config->getWidth(), config->getHeight());
            scene->setBackgroundBrush(Defaults::BOX_COLOR);
            levels.push_back(scene);
            continue;
        }


        QGraphicsItem *madeItem = ItemFactory::make((*config)[i]);
        madeItem->setZValue(zCount);
        if ((*config)[i]->getItemType() == BLOCK) {
            numBlocks[numLevels] ++;
        }
        if ((*config)[i]->getItemType() == PADDLE){
//            std::cout << "i see the paddle" << std::endl;
            m_paddle = Paddle::getPaddle(0);
            continue;
        }
        madeItem->setZValue(zCount++);
        scene->addItem(madeItem);


    }
//    std::cout << numLevels << ": this is the num of levles" << std::endl;
//    for (int i = 0; i < levels.size(); i++){
//        levels[i]->addItem(Paddle::getPaddle(0));
//    }

    levels[0]->addItem(Paddle::getPaddle(0));
    view = new QGraphicsView(this);
    view->setScene(levels[currLevel]);
    view->setRenderHint(QPainter::Antialiasing);

    ui->ScoreLabel->raise();
    ui->Scoreboard->raise();
    ui->LivesLabel->raise();
    ui->LivesBoard->raise();
    ui->LivesBoard->setText(QString::fromStdString(intToString(Defaults::LIVES)));

    ui->LivesBoard->activateWindow();
//    QTimer timer;
    QObject::connect(&paddleTimer, SIGNAL(timeout()), this, SLOT(nextFrame()));
    QObject::connect(&timer, SIGNAL(timeout()), this->getScene(), SLOT(advance()));
    timer.start(1000 / 120);
    paddleTimer.start(1000 / 120);
    gameStart = true;
    QSound::play("sounds/Windows XP Startup.wav");
}



/**
 * @brief Dialog::mousePressEvent stores the
 * @param event
 */
void Dialog::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        BlockConfigItem *newConfig = new BlockConfigItem();

        newConfig->setXCoordinate(event->x() - (newConfig->getWidth() / 2.0));
        newConfig->setYCoordinate(event->y() - (newConfig->getHeight() / 2.0));

        QGraphicsItem *block = ItemFactory::make(newConfig);
        block->setZValue(zCount++);
        levels[currLevel]->addItem(block);
    } else if (event->button() == Qt::RightButton) {
        BallConfigItem *newConfig = new BallConfigItem();

        newConfig->setXCoordinate(event->x() - (newConfig->getRadius()));
        newConfig->setYCoordinate(event->y() - (newConfig->getRadius()));

        QGraphicsItem *ball = ItemFactory::make(newConfig);
        ball->setZValue(zCount++);
        levels[currLevel]->addItem(ball);
    }
}

void Dialog::keyPressEvent(QKeyEvent *event){
    if (event->key() == Qt::Key_A){
        m_paddle->move_left(true);
    }
    else if (event->key() == Qt::Key_D){
        m_paddle->move_right(true);
    }

}

void Dialog::keyReleaseEvent(QKeyEvent *event){
    if (event->key() == Qt::Key_A){
        m_paddle->move_left(false);
    }
    else if (event->key() == Qt::Key_D){
        m_paddle->move_right(false);
    }
}

std::string Dialog::intToString(int input){
    std::ostringstream convert;   // stream used for the conversion
    convert << input;      // insert the textual representation of 'Number' in the characters in the stream
    return convert.str(); // set 'Result' to the contents of the stream
}

void Dialog::update(int livesLeft, int boxCheck)
{
    if (!gameStart){
        boxflag = boxCheck;
        return;
    }
    ui->LivesBoard->setText(QString::fromStdString(intToString(livesLeft)));

//    std::cout <<  currLevel << " : this is the current level" << std::endl;
//    ui->LivesLeft->setText(intToString(livesLeft));
//    lives = livesLeft;
    if (livesLeft == 0){
//        std::cout << "You can't play for shit" << std::endl;
        QSound::play("sounds/Windows XP Critical Stop.wav");
        exit(0);
    }

    if (boxCheck < boxflag){
        boxflag = boxCheck;
        numBlocks[currLevel] --;
        points ++;
        QSound::play("sounds/Windows XP Information Bar.wav");
    } else if (boxCheck > boxflag){
        boxflag = boxCheck;
        numBlocks[currLevel] ++;
    }
//    std::cout << "number of blocks left: " << numBlocks[currLevel];
   if (numBlocks[currLevel] <= 0){

       if (currLevel < numLevels){
          currLevel ++;
          levels[currLevel]->addItem(m_paddle);
          timer.disconnect(this->getScene(), SLOT(advance()));
          QObject::disconnect(&timer, 0,0,0);
          view->setScene(levels[currLevel]);

          QObject::disconnect(&timer, SIGNAL(timeout()), this->getScene(), SLOT(advance()));
          QObject::connect(&timer, SIGNAL(timeout()), this->getScene(), SLOT(advance()));
       }else{
           ui->LivesBoard->setText("YOU WIN!!!");
       }

//       std::cout << "you win!" << std::endl;
   }
   ui->Scoreboard->setText(QString::fromStdString(intToString(points)));

}

void Dialog::nextFrame()
{
    int xpos = m_paddle->x();
    if (m_paddle->left()){
        if (xpos > 5 ){
            m_paddle->setX(xpos - 5);
        }else{
            if (Defaults::SOUNDON)
                QSound::play("sounds/Windows XP Ding.wav");
        }
    }
    else if (m_paddle->right()){
        if (xpos < int(levels[currLevel]->width()- m_paddle->getWidth()) ){
            m_paddle->setX(m_paddle->x() + 5);
        }else{
            if (Defaults::SOUNDON)
                QSound::play("sounds/Windows XP Ding.wav");
        }

    }
//    update();
}

Dialog::~Dialog()
{
    delete ui;
    delete view;
    //when deleting scene, scene also deletes all other items associated with the scene (i.e the ball)
    for (int i = 0; i < levels.size(); i++){
        delete levels[i];
    }
}

QGraphicsScene * Dialog::getScene()
{

    return levels[currLevel];
}
