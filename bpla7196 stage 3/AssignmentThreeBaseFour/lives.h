#ifndef LIVES_H
#define LIVES_H
#include "observerable.h"
#include <string>
#include <QSound>

/*!
 * \brief The Lives class
 * Concrete implementation of an Observerable. It also
 * contains information about the number of bricks. This
 * is a flag so that Dialog will know if bricks have been
 * destroyed upon update.
 *
 * The inclusion of more than lives in the observerable was
 * a decision made for greater extendability in future code.
 *
 * If 2 concrete implementations of observerable were made,
 * and both updated the same class (in this case, dialog),
 * they would both have to call the update method, with the same
 * definition (same arguments so can't overload update(int)).
 *
 * The only solution would be to have many update methods, and have
 * one abstract for one concrete (instead of now, with one abstract
 * for all concretes).
 */

class Lives : public Observerable
{
public:
    static Lives* getInstance(int lives);

    Lives(int numLives);
    // Observerable interface
    void attatch(Observer &observer);
    void detatch(Observer &observer);
    void notify();
    void incrementBricks();
    void decrementBricks();
    void decrementLives();

private:
    static int lives;
    static int numBricks;
    static Lives* instance;
};

#endif // LIVES_H
