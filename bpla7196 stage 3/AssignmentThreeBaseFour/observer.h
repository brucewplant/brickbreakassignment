#ifndef OBSERVER_H
#define OBSERVER_H
/*!
 * \brief The Observer class
 * Abstract class for implementing the observer pattern.
 * Observers in this implementation update 2 variables, the
 * livesLeft and the number of blocks.
 *
 * Number of blocks contains the number of blocks in the entire
 * game, not just on one level. It is more used as a flag to
 * determine if a block has just died (a reduction indicates so).
 */
class Observer
{
public:
    virtual void update(int livesLeft, int numBlocks) = 0;
};

#endif // OBSERVER_H
