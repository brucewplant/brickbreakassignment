#include "itemfactory.h"

QGraphicsItem* ItemFactory::make(ConfigItem *config)
{
    switch (config->getItemType()) {
    case BALL:
        return new Ball((BallConfigItem *)config);
        break;
    case BLOCK:
        return new Block((BlockConfigItem *)config);
        break;
    case PADDLE:
        return Paddle::getPaddle((PaddleConfigItem *)config);
        break;
    default:
        return NULL;
    }
}
