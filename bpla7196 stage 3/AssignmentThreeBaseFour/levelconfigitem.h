#ifndef LEVELCONFIGITEM_H
#define LEVELCONFIGITEM_H
#include "configitem.h"

/*!
 * \brief The LevelConfigItem class
 * This is really not anything important. LevelConfigItem is
 * merely used as a separator for levels. This way, the ConfigItems
 * can be read uniformly, and when "[LEVEL]" is hit, can just include
 * it in the list of config items.
 *
 * In Dialog, hitting a LevelConfigItem will cause items underneath it
 * to the next LevelConfigItem to be placed in a different scene.
 *
 * Doing it this way may not make common sense, but it makes implementation
 * easier by not having to make egrigious changes to the way the config file is
 * parsed. Subsequently, it is recommended for similar changes by other
 * programmers to be done in a similar way.
 */
class LevelConfigItem : public ConfigItem
{
public:
    LevelConfigItem();
    ~LevelConfigItem();
    // ConfigItem interface
public:
    virtual bool validate(int height, int width) const;
    virtual Item getItemType() const;
    virtual void addParameter(std::string name, double value);
};

#endif // LEVELCONFIGITEM_H
